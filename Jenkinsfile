pipeline {
    agent any
    environment {
        DOCKER_ACCOUNT = credentials('robot_account')
        DOCKER_URL = 'docker.jala.pro'
        DOCKER_TAG = "${env.GIT_COMMIT}"
        DEV_TARGET_SERVER = 'ssh://ubuntu@10.24.48.213'
        UAT_TARGET_SERVER = 'ssh://ubuntu@10.24.48.148'
        PROD_TARGET_SERVER = 'ssh://ubuntu@10.24.48.111'
        DEPLOY_TAG = '1.0.0'
        DEV_ENV = 'dev'
        UAT_ENV = 'uat'
        PROD_ENV = 'prod'
    }
    triggers {
        pollSCM('H/2 * * * *')
    }

    stages {
        stage('Commit') {

            steps {
                echo 'Commit...'
                sh "docker build -t ${DOCKER_URL}/devops/dc_covid19-tracker:${DOCKER_TAG} ."
            }
        }
        stage('Publish') {
            when {
                branch 'develop'
            }
            steps {
                sh 'docker login -u ${DOCKER_ACCOUNT_USR} -p ${DOCKER_ACCOUNT_PSW} "${DOCKER_URL}"'
                sh "docker push ${DOCKER_URL}/devops/dc_covid19-tracker:${DOCKER_TAG}"
            }
            post {
                always {
                    sh "docker container rm -f ${DOCKER_URL}/devops/dc_covid19-tracker:${DOCKER_TAG}"
                }
            }
        }
        stage ('Deploy') {
            when {
                branch 'develop'
            }
            steps {
                sh 'docker context create ${DEV_ENV} --docker "host=${DEV_TARGET_SERVER}"'
                sh 'docker --context ${DEV_ENV} stack deploy -c docker-compose.dev.yml covid19'
            }
            post {
                always {
                    sh 'docker context rm ${DEV_ENV}'
                }
            }
        }
        stage('Acceptance') {
            when {
                branch 'develop'
            }
            steps {
                build job: "dc_demo-ui-acceptance/master", propagate: true, wait: true
            }
        }
        stage('Capacity') {
            when {
                branch 'develop'
            }
            steps {
                build job: "dc_gatling-gradle-plugin-demo/master", propagate: true, wait: true
            }
        }
        stage ('UAT') {
            when {
                branch 'master'
            }
            steps {
                sh 'docker context create ${UAT_ENV} --docker "host=${UAT_TARGET_SERVER}"'
                sh 'docker --context ${UAT_ENV} stack deploy -c docker-compose.uat.yml covid19'
            }
            post {
                always {
                    sh 'docker context rm ${UAT_ENV}'
                }
            }
        }
        stage ('Promote') {
            when {
                branch 'master'
            }
            steps {
                sh 'docker context create ${DEV_ENV} --docker "host=${DEV_TARGET_SERVER}"'
                sh 'docker --context ${DEV_ENV} stack deploy -c docker-compose.dev.yml covid19'
            }
            post {
                always {
                    sh 'docker context rm ${DEV_ENV}'
                }
            }
        }
        stage ('Production') {
            when {
                branch 'master'
            }
            steps {
                sh 'docker context create ${PROD_ENV} --docker "host=${PROD_TARGET_SERVER}"'
                sh 'docker --context ${PROD_ENV} stack deploy -c docker-compose.prod.yml covid19'
            }
            post {
                always {
                    sh 'docker context rm ${PROD_ENV}'
                }
            }
        }
    }
}
