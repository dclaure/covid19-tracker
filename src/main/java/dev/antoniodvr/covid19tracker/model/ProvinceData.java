package dev.antoniodvr.covid19tracker.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "stato",
        "codice_regione",
        "denominazione_regione",
        "codice_provincia",
        "denominazione_provincia",
        "sigla_provincia",
        "lat",
        "long",
        "totale_casi"
})
public class ProvinceData implements Auditable {

    @JsonProperty("data")
    private String date;
    @JsonProperty("stato")
    private String country;
    @JsonProperty("codice_regione")
    private Integer regionCode;
    @JsonProperty("denominazione_regione")
    private String regionName;
    @JsonProperty("codice_provincia")
    private Integer provinceCode;
    @JsonProperty("denominazione_provincia")
    private String provinceName;
    @JsonProperty("sigla_provincia")
    private String provinceShortName;
    @JsonProperty("lat")
    private Float latitude;
    @JsonProperty("long")
    private Float longitude;
    @JsonProperty("totale_casi")
    private Integer totalCases;

    @Override
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(Integer regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Integer getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(Integer provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getProvinceShortName() {
        return provinceShortName;
    }

    public void setProvinceShortName(String provinceShortName) {
        this.provinceShortName = provinceShortName;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Integer getTotalCases() {
        return totalCases;
    }

    public void setTotalCases(Integer totalCases) {
        this.totalCases = totalCases;
    }
}