package dev.antoniodvr.covid19tracker.model;

public interface Auditable {

    String getDate();

}
